#! /bin/sh -
docker-compose build
docker-compose up -d --scale chrome=3
docker ps
docker system prune -f
EXIT_CODE=$(docker wait cucumber_tester_1)
docker logs cucumber_tester_1
docker-compose stop
exit $EXIT_CODE