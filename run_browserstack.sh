#! /bin/sh -
docker-compose -f docker-compose.browserstack.yml build
docker-compose -f docker-compose.browserstack.yml up -d
docker ps
EXIT_CODE=$(docker wait cucumber_tester_1)
docker logs cucumber_tester_1
docker-compose -f docker-compose.browserstack.yml stop
exit $EXIT_CODE
